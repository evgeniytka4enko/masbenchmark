//
//  UIImage+GWAdditions.m
//  GraduateWork
//
//  Created by Evgeniy on 13.04.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "UIImage+GWAdditions.h"

@implementation UIImage (GWAdditions)

#pragma mark - Public

+ (UIImage *)imageWithBezierPath:(UIBezierPath *)bezierPath fillColor:(UIColor *)fillColor strokeColor:(UIColor *)strokeColor
{
    UIImage *image = nil;
    if(bezierPath)
    {
        UIGraphicsBeginImageContextWithOptions(bezierPath.bounds.size, NO, [UIScreen mainScreen].scale);
        if(fillColor)
        {
            [fillColor setFill];
            [bezierPath fill];
        }
        if(strokeColor)
        {
            [strokeColor setStroke];
            [bezierPath stroke];
        }
        image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return image;
}

@end
