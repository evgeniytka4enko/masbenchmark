//
//  GWConstants.m
//  GraduateWork
//
//  Created by Evgeniy on 10.03.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWConstants.h"

CGFloat const GWConstantsUnitSize = 5.0f;
CGFloat const GWConstantsBaseSize = 30.0f;
CGFloat const GWConstantsResourceSize = 10.0f;
NSUInteger const GWConstantsFPS = 60;
