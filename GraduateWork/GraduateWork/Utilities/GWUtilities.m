//
//  GWUtilities.m
//  GraduateWork
//
//  Created by Evgeniy on 19.03.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWUtilities.h"

CGFloat CGPointDistance(CGPoint p1, CGPoint p2)
{
    return sqrtf(powf(p1.x - p2.x, 2) + powf(p1.y - p2.y, 2));
}

@implementation GWUtilities

@end
