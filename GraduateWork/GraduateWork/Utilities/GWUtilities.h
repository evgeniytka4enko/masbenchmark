//
//  GWUtilities.h
//  GraduateWork
//
//  Created by Evgeniy on 19.03.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <Foundation/Foundation.h>

CGFloat CGPointDistance(CGPoint p1, CGPoint p2);

@interface GWUtilities : NSObject

@end
