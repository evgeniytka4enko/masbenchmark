//
//  UIImage+GWAdditions.h
//  GraduateWork
//
//  Created by Evgeniy on 13.04.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (GWAdditions)

+ (UIImage *)imageWithBezierPath:(UIBezierPath *)bezierPath fillColor:(UIColor *)fillColor strokeColor:(UIColor *)strokeColor;

@end
