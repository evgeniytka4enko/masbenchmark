//
//  GWConstants.h
//  GraduateWork
//
//  Created by Evgeniy on 10.03.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

extern CGFloat const GWConstantsUnitSize;
extern CGFloat const GWConstantsBaseSize;
extern CGFloat const GWConstantsResourceSize;
extern NSUInteger const GWConstantsFPS;