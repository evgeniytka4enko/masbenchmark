//
//  GWGameViewController.m
//  GraduateWork
//
//  Created by Evgeniy on 19.01.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWGameViewController.h"
#import "GWGameScene.h"
#import "GWGameResultsViewController.h"

@interface GWGameViewController () <GWGameSceneDelegate>

@property (strong, nonnull) GWGameParameters *gameParameters;

@end

@implementation GWGameViewController

#pragma mark - View lifecycle

- (instancetype)initWithGameParameters:(GWGameParameters *)gameParameters
{
    self = [super initWithNibName:nil bundle:nil];
    if(self)
    {
        self.gameParameters = gameParameters;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    SKView *view = (SKView *)self.view;
    
    view.showsFPS = YES;
    view.showsNodeCount = YES;
    view.ignoresSiblingOrder = YES;
    
    if(self.gameParameters.mapIdx)
    {
        srand(self.gameParameters.mapIdx.unsignedIntegerValue);
    }
    else
    {
        srand(arc4random());
    }
    
    GWGameScene *gameScene = [GWGameScene sceneWithSize:[UIScreen mainScreen].bounds.size gameParameters:self.gameParameters];
    gameScene.gameSceneDelegate = self;
    [view presentScene:gameScene];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GWGameSceneDelegate

- (void)gameScene:(GWGameScene *)gameScene didFinishWithGameResults:(GWGameResults *)gameResults
{
    GWGameResultsViewController *gameResultsViewController = [[GWGameResultsViewController alloc] initWithGameResults:gameResults];
    
    NSMutableArray<UIViewController *> *viewControllers = [self.navigationController.viewControllers mutableCopy];
    [viewControllers removeLastObject];
    [viewControllers addObject:gameResultsViewController];
    [self.navigationController setViewControllers:viewControllers animated:YES];
}

@end
