//
//  GWUnitNode.h
//  GraduateWork
//
//  Created by Evgeniy on 27.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWGameNode.h"
#import "GWUnitAI.h"

@interface GWUnitNode : GWGameNode

@property (readonly, nonatomic) GWUnitAI *unitAI;

+ (instancetype)nodeWithIdx:(NSNumber *)idx step:(NSNumber *)step visibilityRadius:(NSNumber *)visibilityRadius unitAI:(GWUnitAI *)unitAI;
- (instancetype)initWithIdx:(NSNumber *)idx step:(NSNumber *)step visibilityRadius:(NSNumber *)visibilityRadius unitAI:(GWUnitAI *)unitAI;

@end
