//
//  GWGameScene.h
//  GraduateWork
//
//  Created by Evgeniy on 03.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GWGameParameters.h"
#import "GWGameResults.h"

@class GWGameScene;

@protocol GWGameSceneDelegate <NSObject>
@optional
- (void)gameScene:(GWGameScene *)gameScene didFinishWithGameResults:(GWGameResults *)gameResults;

@end

@interface GWGameScene : SKScene

@property (weak, nonatomic) id<GWGameSceneDelegate> gameSceneDelegate;
@property (readonly, nonatomic) GWGameParameters *gameParameters;

+ (instancetype)sceneWithSize:(CGSize)size gameParameters:(GWGameParameters *)gameParameters;
- (instancetype)initWithSize:(CGSize)size gameParameters:(GWGameParameters *)gameParameters;

@end
