//
//  GWGameNodeSubclass.h
//  GraduateWork
//
//  Created by Evgeniy on 12.04.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

@interface GWGameNode (GWGameNodeProtected)

@property (strong, nonatomic) NSNumber *idx;
@property (strong, nonatomic) NSNumber *step;

@end
