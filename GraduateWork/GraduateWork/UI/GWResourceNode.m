//
//  GWResourceNode.m
//  GraduateWork
//
//  Created by Evgeniy on 19.03.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWResourceNode.h"

@implementation GWResourceNode

#pragma mark - Initialization

+ (instancetype)nodeWithIdx:(NSNumber *)idx step:(NSNumber *)step
{
    return [[self alloc] initWithIdx:idx step:step];
}

- (instancetype)initWithIdx:(NSNumber *)idx step:(NSNumber *)step
{
    self = [super initWithIdx:idx step:step];
    if(self)
    {
        [self setupResourceNode];
    }
    return self;
}

#pragma mark - Private

- (void)setupResourceNode
{
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0.0f, 0.0f, GWConstantsResourceSize, GWConstantsResourceSize)];
    self.texture = [SKTexture textureWithImage:[UIImage imageWithBezierPath:bezierPath fillColor:[[UIColor brownColor] colorWithAlphaComponent:0.5f] strokeColor:nil]];
    self.size = self.texture.size;
}

@end
