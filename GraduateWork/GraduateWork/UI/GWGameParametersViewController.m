//
//  GWGameParametersViewController.m
//  GraduateWork
//
//  Created by Evgeniy on 19.04.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWGameParametersViewController.h"
#import "GWGameViewController.h"
#import "GWDecentralizedUnitsAI.h"
#import "GWCentralizedUnitsAI.h"

@interface GWGameParametersViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *AITypeSegmentedControl;
@property (weak, nonatomic) IBOutlet UITextField *mapIdxTextField;
@property (weak, nonatomic) IBOutlet UITextField *basesCountTextField;
@property (weak, nonatomic) IBOutlet UITextField *unitsCountTextField;
@property (weak, nonatomic) IBOutlet UITextField *unitVisibilityRadiusTextField;
@property (weak, nonatomic) IBOutlet UITextField *resourcesCountTextField;
@property (weak, nonatomic) IBOutlet UITextField *resourceStepsCountTextField;
@property (weak, nonatomic) IBOutlet UITextField *speedTextField;

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFields;

@end

@implementation GWGameParametersViewController

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Параметры";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    
    NSValue *endFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardEndFrame = endFrameValue.CGRectValue;
    
    NSNumber *durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = durationValue.doubleValue;
    
    NSNumber *curveValue = userInfo[UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = curveValue.intValue;
    
    [UIView animateWithDuration:animationDuration delay:0.0 options:(animationCurve << 16) animations:^{
        
        UIEdgeInsets insets = self.scrollView.contentInset;
        insets.bottom = keyboardEndFrame.size.height;
        self.scrollView.contentInset = self.scrollView.scrollIndicatorInsets = insets;
        
    } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    
    NSNumber *durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = durationValue.doubleValue;
    
    NSNumber *curveValue = userInfo[UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = curveValue.intValue;
    
    [UIView animateWithDuration:animationDuration delay:0.0 options:(animationCurve << 16) animations:^{
        
        UIEdgeInsets insets = self.scrollView.contentInset;
        insets.bottom = 0.0f;
        self.scrollView.contentInset = self.scrollView.scrollIndicatorInsets = insets;
        
    } completion:nil];
}

- (IBAction)textFieldEditingChanged:(id)sender
{
    UITextField *textField = (UITextField *)sender;
    
    textField.text = [[textField.text componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
}

- (IBAction)startButtonPressed:(id)sender
{
    GWGameParameters *gameParameters = [[GWGameParameters alloc] init];
    
    if(self.AITypeSegmentedControl.selectedSegmentIndex == 0)
    {
        gameParameters.unitsAI = [[GWCentralizedUnitsAI alloc] initWithSize:[UIScreen mainScreen].bounds.size];
    }
    else
    {
        gameParameters.unitsAI = [[GWDecentralizedUnitsAI alloc] initWithSize:[UIScreen mainScreen].bounds.size];
    }
    NSString *mapIdxString = self.mapIdxTextField.text;
    if(mapIdxString.length > 0)
    {
        gameParameters.mapIdx = @(mapIdxString.integerValue);
    }
    NSString *basesCountString = self.basesCountTextField.text;
    if(basesCountString.length > 0)
    {
        gameParameters.basesCount = @(basesCountString.integerValue);
    }
    NSString *unitsCountString = self.unitsCountTextField.text;
    if(unitsCountString.length > 0)
    {
        gameParameters.unitsCount = @(unitsCountString.integerValue);
    }
    NSString *unitVisibilityRadiusString = self.unitVisibilityRadiusTextField.text;
    if(unitVisibilityRadiusString.length > 0)
    {
        gameParameters.unitVisibilityRadius = @(unitVisibilityRadiusString.integerValue);
    }
    NSString *resourcesCountString = self.resourcesCountTextField.text;
    if(resourcesCountString.length > 0)
    {
        gameParameters.resourcesCount = @(resourcesCountString.integerValue);
    }
    NSString *resourceStepsCountString = self.resourceStepsCountTextField.text;
    if(resourceStepsCountString.length > 0)
    {
        gameParameters.resourceStepsCount = @(GWConstantsFPS * resourceStepsCountString.integerValue);
    }
    NSString *speedString = self.speedTextField.text;
    if(speedString.length > 0)
    {
        gameParameters.speed = @(speedString.integerValue);
    }
    
    GWGameViewController *gameViewController = [[GWGameViewController alloc] initWithGameParameters:gameParameters];
    [self.navigationController pushViewController:gameViewController animated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([self.textFields containsObject:textField])
    {
        if(textField == self.textFields.lastObject)
        {
            [textField resignFirstResponder];
        }
        else
        {
            [self.textFields[[self.textFields indexOfObject:textField] + 1] becomeFirstResponder];
        }
    }
    return YES;
}

@end
