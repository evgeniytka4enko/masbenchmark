//
//  GWBaseNode.m
//  GraduateWork
//
//  Created by Evgeniy on 10.03.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWBaseNode.h"
#import "GWGameNodeSubclass.h"

@interface GWBaseNode ()

@property (assign, nonatomic) NSUInteger resourcesCount;

@end

@implementation GWBaseNode

#pragma mark - Initialization

+ (instancetype)nodeWithIdx:(NSNumber *)idx step:(NSNumber *)step
{
    return [[self alloc] initWithIdx:idx step:step];
}

- (instancetype)initWithIdx:(NSNumber *)idx step:(NSNumber *)step
{
    self = [super initWithIdx:idx step:step];
    if(self)
    {
        [self setupBaseNode];
    }
    return self;
}

#pragma mark - Public

- (void)putResourceWithStep:(NSNumber *)step
{
    self.resourcesCount++;
    self.step = step;
}

#pragma mark - Private

- (void)setupBaseNode
{
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0.0f, 0.0f, GWConstantsBaseSize, GWConstantsBaseSize)];
    self.texture = [SKTexture textureWithImage:[UIImage imageWithBezierPath:bezierPath fillColor:[[UIColor orangeColor] colorWithAlphaComponent:0.5f] strokeColor:nil]];
    self.size = self.texture.size;
}

@end
