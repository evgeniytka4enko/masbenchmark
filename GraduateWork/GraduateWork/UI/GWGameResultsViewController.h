//
//  GWGameResultsViewController.h
//  GraduateWork
//
//  Created by Evgeniy on 23.04.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWGameResults.h"

@interface GWGameResultsViewController : UIViewController

- (instancetype)initWithGameResults:(GWGameResults *)gameResults;

@end
