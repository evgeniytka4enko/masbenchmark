//
//  GWBaseNode.h
//  GraduateWork
//
//  Created by Evgeniy on 10.03.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWGameNode.h"

@interface GWBaseNode : GWGameNode

@property (readonly, nonatomic) NSUInteger resourcesCount;

- (void)putResourceWithStep:(NSNumber *)step;

@end
