//
//  GWUnitNode.m
//  GraduateWork
//
//  Created by Evgeniy on 27.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWUnitNode.h"

@interface GWUnitNode ()

@property (strong, nonatomic) NSNumber *visibilityRadius;
@property (strong, nonatomic) GWUnitAI *unitAI;

@end

@implementation GWUnitNode

#pragma mark - Initialization

+ (instancetype)nodeWithIdx:(NSNumber *)idx step:(NSNumber *)step visibilityRadius:(NSNumber *)visibilityRadius unitAI:(GWUnitAI *)unitAI
{
    return [[self alloc] initWithIdx:idx step:step visibilityRadius:visibilityRadius unitAI:unitAI];
}

- (instancetype)initWithIdx:(NSNumber *)idx step:(NSNumber *)step visibilityRadius:(NSNumber *)visibilityRadius unitAI:(GWUnitAI *)unitAI
{
    self = [super initWithIdx:idx step:step];
    if(self)
    {
        self.visibilityRadius = visibilityRadius;
        self.unitAI = unitAI;
        
        [self setupUnitNode];
    }
    return self;
}

#pragma mark - Private

- (void)setupUnitNode
{
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0.0f, 0.0f, GWConstantsUnitSize, GWConstantsUnitSize)];
    self.texture = [SKTexture textureWithImage:[UIImage imageWithBezierPath:bezierPath fillColor:[UIColor redColor] strokeColor:nil]];
    self.size = self.texture.size;
    
    if(self.visibilityRadius)
    {
        SKSpriteNode *radiusNode = [SKSpriteNode node];
        
        CGFloat radiusNodeSize = (self.visibilityRadius.floatValue * 2);
        bezierPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0.0f, 0.0f, radiusNodeSize, radiusNodeSize)];
        radiusNode.texture = [SKTexture textureWithImage:[UIImage imageWithBezierPath:bezierPath fillColor:[[UIColor blueColor] colorWithAlphaComponent:0.25f] strokeColor:nil]];
        radiusNode.size = radiusNode.texture.size;
        
        [self addChild:radiusNode];
    }
}

@end
