//
//  GWGameNode.m
//  GraduateWork
//
//  Created by Evgeniy on 10.03.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWGameNode.h"

@interface GWGameNode ()

@property (strong, nonatomic) NSNumber *idx;
@property (strong, nonatomic) NSNumber *step;

@end

@implementation GWGameNode

#pragma mark - Initialization

+ (instancetype)nodeWithIdx:(NSNumber *)idx step:(NSNumber *)step
{
    return [[self alloc] initWithIdx:idx step:step];
}

- (instancetype)initWithIdx:(NSNumber *)idx step:(NSNumber *)step
{
    self = [super init];
    if(self)
    {
        self.idx = idx;
        self.step = step;
    }
    return self;
}

@end
