//
//  GWGameViewController.h
//  GraduateWork
//
//  Created by Evgeniy on 19.01.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWGameParameters.h"

@interface GWGameViewController : UIViewController

- (instancetype)initWithGameParameters:(GWGameParameters *)gameParameters;

@end
