//
//  GWGameResultsViewController.m
//  GraduateWork
//
//  Created by Evgeniy on 23.04.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWGameResultsViewController.h"

typedef NS_ENUM(NSUInteger, GWGameResultsViewControllerSectionType)
{
    GWGameResultsViewControllerSectionTypeCommonResults,
    GWGameResultsViewControllerSectionTypeUnitsResults,
    GWGameResultsViewControllerSectionTypeBasesResults,
    GWGameResultsViewControllerSectionTypeCount
};

@interface GWGameResultsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) GWGameResults *gameResults;

@end

@implementation GWGameResultsViewController

#pragma mark - View lifecycle

- (instancetype)initWithGameResults:(GWGameResults *)gameResults
{
    self = [super initWithNibName:nil bundle:nil];
    if(self)
    {
        self.gameResults = gameResults;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Результаты";
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return GWGameResultsViewControllerSectionTypeCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch(section)
    {
        case GWGameResultsViewControllerSectionTypeCommonResults:
        {
            return 3;
        }
            
        case GWGameResultsViewControllerSectionTypeUnitsResults:
        {
            return self.gameResults.unitsResults.count;
        }
            
        case GWGameResultsViewControllerSectionTypeBasesResults:
        {
            return self.gameResults.basesResults.count;
        }
            
        default:
        {
            return 0;
        }
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch(section)
    {
        case GWGameResultsViewControllerSectionTypeCommonResults:
        {
            return @"Общие";
        }
            
        case GWGameResultsViewControllerSectionTypeUnitsResults:
        {
            return @"Юниты";
        }
            
        case GWGameResultsViewControllerSectionTypeBasesResults:
        {
            return @"Базы";
        }
            
        default:
        {
            return nil;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
    if(!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:NSStringFromClass([UITableViewCell class])];
    }
    
    switch(indexPath.section)
    {
        case GWGameResultsViewControllerSectionTypeCommonResults:
        {
            switch(indexPath.row)
            {
                case 0:
                {
                    cell.textLabel.text = @"Количество шагов";
                    cell.detailTextLabel.text = self.gameResults.finishStep.stringValue;
                    break;
                }
                    
                case 1:
                {
                    cell.textLabel.text = @"Время принятия решений";
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.03lf", self.gameResults.summaryDecideDuration.doubleValue];
                    break;
                }
                    
                case 2:
                {
                    cell.textLabel.text = @"Среднее время прин. реш.";
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%.03lf", (self.gameResults.summaryDecideDuration.doubleValue / self.gameResults.finishStep.unsignedIntegerValue)];
                    break;
                }
                    
                default:
                {
                    break;
                }
            }
            break;
        }
            
        case GWGameResultsViewControllerSectionTypeUnitsResults:
        {
            GWUnitResults *unitResults = self.gameResults.unitsResults[indexPath.row];
            
            cell.textLabel.text = [NSString stringWithFormat:@"#%i", (indexPath.row + 1)];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ рес.", unitResults.puttedResourcesCount];
            break;
        }
            
        case GWGameResultsViewControllerSectionTypeBasesResults:
        {
            GWBaseResults *baseResults = self.gameResults.basesResults[indexPath.row];
            
            cell.textLabel.text = [NSString stringWithFormat:@"#%i", (indexPath.row + 1)];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ рес.", baseResults.resourcesCount];
            break;
        }
            
        default:
        {
            break;
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

@end
