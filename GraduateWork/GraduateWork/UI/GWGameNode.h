//
//  GWGameNode.h
//  GraduateWork
//
//  Created by Evgeniy on 10.03.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GWGameNode : SKSpriteNode

@property (readonly, nonatomic) NSNumber *idx;
@property (readonly, nonatomic) NSNumber *step;

+ (instancetype)nodeWithIdx:(NSNumber *)idx step:(NSNumber *)step;

- (instancetype)initWithIdx:(NSNumber *)idx step:(NSNumber *)step;

@end
