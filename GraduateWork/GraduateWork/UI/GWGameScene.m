//
//  GWGameScene.m
//  GraduateWork
//
//  Created by Evgeniy on 03.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWGameScene.h"
#import "GWUnitsAI.h"
#import "GWUnitNode.h"
#import "GWBaseNode.h"
#import "GWResourceNode.h"
#import "GWDecentralizedUnitsAI.h"
#import "GWUnitAIResourceFact.h"
#import "GWUnitAIBaseFact.h"

CGFloat const GWGameSceneUnitNodeTopOffset = 50.0f;
CGFloat const GWGameSceneUnitNodeBottomOffset = 75.0f;
CGFloat const GWGameSceneResourceNodeHorisontalOffset = 10.0f;
NSUInteger const GWGameSceneUnitNodesHorisontalCount = 5;

@interface GWGameScene ()

@property (strong, nonatomic) NSNumber *step;
@property (strong, nonatomic) NSNumber *idx;

@property (strong, nonatomic) GWGameParameters *gameParameters;
@property (strong, nonatomic) GWGameResults *gameResults;

@property (strong, nonatomic) NSMutableArray<GWResourceNode *> *resourceNodes;
@property (strong, nonatomic) NSMutableDictionary<NSNumber *, NSArray<GWUnitAIFact *> *> *unitIdxsWithKnownFacts;

@end

@implementation GWGameScene

#pragma mark - Initialization

+ (instancetype)sceneWithSize:(CGSize)size gameParameters:(GWGameParameters *)gameParameters
{
    return [[self alloc] initWithSize:size gameParameters:gameParameters];
}

- (instancetype)initWithSize:(CGSize)size gameParameters:(GWGameParameters *)gameParameters
{
    self = [super initWithSize:size];
    if(self)
    {
        self.gameParameters = gameParameters;
        
        [self setupGameScene];
    }
    return self;
}

#pragma mark - Custom accessors

- (NSNumber *)idx
{
    if(!_idx)
    {
        _idx = @0;
    }
    return _idx;
}

- (NSMutableArray<GWResourceNode *> *)resourceNodes
{
    if(!_resourceNodes)
    {
        _resourceNodes = [NSMutableArray array];
    }
    return _resourceNodes;
}

- (NSMutableDictionary<NSNumber *,NSArray<GWUnitAIFact *> *> *)unitIdxsWithKnownFacts
{
    if(!_unitIdxsWithKnownFacts)
    {
        _unitIdxsWithKnownFacts = [NSMutableDictionary dictionary];
    }
    return _unitIdxsWithKnownFacts;
}

- (GWGameResults *)gameResults
{
    if(!_gameResults)
    {
        _gameResults = [[GWGameResults alloc] init];
    }
    return _gameResults;
}

#pragma mark - Overrides

- (void)update:(NSTimeInterval)currentTime
{
    [self.unitIdxsWithKnownFacts removeAllObjects];
    
    NSDate *learnFactsDate = [NSDate date];
    for(GWUnitNode *unitNode in self.unitNodes)
    {
        [unitNode.unitAI learnFacts:[self factsForUnitNode:unitNode]];
    }
    self.gameResults.summaryLearnFactsDuration = @(self.gameResults.summaryLearnFactsDuration.doubleValue + [[NSDate date] timeIntervalSinceDate:learnFactsDate]);
    
    NSDate *decideDate = [NSDate date];
    for(GWUnitNode *unitNode in self.unitNodes)
    {
        [unitNode.unitAI setPosition:unitNode.position step:self.step];
    }
    for(GWUnitNode *unitNode in self.unitNodes)
    {
        CGPoint actionPosition = unitNode.unitAI.AIAction.position;
        [unitNode.unitAI decide];
        if(CGPointDistance(actionPosition, unitNode.unitAI.AIAction.position) >= 1.0f)
        {
            NSTimeInterval duration = (CGPointDistance(unitNode.position, unitNode.unitAI.AIAction.position) / GWConstantsFPS / (self.gameParameters.speed ? self.gameParameters.speed.unsignedIntegerValue : 1));
            
            [unitNode removeAllActions];
            [unitNode runAction:[SKAction moveTo:unitNode.unitAI.AIAction.position duration:duration]];
        }
    }
    
    for(GWUnitNode *unitNode in self.unitNodes)
    {
        if(CGPointDistance(unitNode.position, unitNode.unitAI.AIAction.position) < 1.0f)
        {
            [self performAIActionWithUnitNode:unitNode];
        }
    }
    self.gameResults.summaryDecideDuration = @(self.gameResults.summaryDecideDuration.doubleValue + [[NSDate date] timeIntervalSinceDate:decideDate]);
    
    [self generateResources];
    
    [self tryFinishGame];
    
    self.step = @(self.step.unsignedIntegerValue + 1);
}

#pragma mark - Private

- (void)setupGameScene
{
    self.backgroundColor = [UIColor whiteColor];
    
    [self setupBaseNodes];
    [self setupUnitNodes];
}

- (void)setupUnitNodes
{
    CGFloat xOffset = (self.size.width / (GWGameSceneUnitNodesHorisontalCount + 1));
    
    NSUInteger linesCount = (self.gameParameters.unitsCount.unsignedIntegerValue / GWGameSceneUnitNodesHorisontalCount + (self.gameParameters.unitsCount.unsignedIntegerValue % GWGameSceneUnitNodesHorisontalCount > 0 ? 1 : 0));
    CGFloat yOffset = ((self.size.height - GWGameSceneUnitNodeTopOffset - GWGameSceneUnitNodeBottomOffset) / linesCount);
    for(NSUInteger i = 0; i < self.gameParameters.unitsCount.unsignedIntegerValue; i++)
    {
        NSNumber *idx = [self nextIdx];
        CGPoint position = CGPointMake(xOffset + xOffset * (i % GWGameSceneUnitNodesHorisontalCount), (yOffset / 2) + yOffset * (i / GWGameSceneUnitNodesHorisontalCount) + GWGameSceneUnitNodeBottomOffset);
        
        GWUnitAI *unitAI = [self.gameParameters.unitsAI createUnitAIWithIdx:idx];
        
        GWUnitNode *unitNode = [[GWUnitNode alloc] initWithIdx:idx step:self.step visibilityRadius:@(self.gameParameters.unitVisibilityRadius.floatValue) unitAI:unitAI];
        unitNode.position = position;
        
        [self addChild:unitNode];
    }
}

- (void)setupBaseNodes
{
    CGFloat yOffset = (GWConstantsBaseSize / 2);
    CGFloat xOffset = (self.size.width / (self.gameParameters.basesCount.unsignedIntegerValue + 1));
    
    for(NSUInteger i = 0; i < self.gameParameters.basesCount.unsignedIntegerValue; i++)
    {
        GWBaseNode *baseNode = [[GWBaseNode alloc] initWithIdx:[self nextIdx] step:self.step];
        baseNode.position = CGPointMake(xOffset * (i + 1), yOffset);
        
        [self addChild:baseNode];
    }
}

- (NSNumber *)nextIdx
{
    NSNumber *nextIdx = self.idx;
    self.idx = @(self.idx.unsignedIntegerValue + 1);
    return nextIdx;
}

- (void)generateResources
{
    void(^createResourceNodeBlock)() = ^{
        
        GWResourceNode *resourceNode = [[GWResourceNode alloc] initWithIdx:[self nextIdx] step:self.step];
        
        CGFloat height = (self.size.height - GWGameSceneUnitNodeTopOffset - GWGameSceneUnitNodeBottomOffset);
        CGFloat x = (rand() % (NSUInteger)(self.size.width - GWGameSceneResourceNodeHorisontalOffset * 2) + GWGameSceneResourceNodeHorisontalOffset);
        CGFloat y = (rand() % (NSUInteger)height + GWGameSceneUnitNodeBottomOffset);
        
        resourceNode.position = CGPointMake(x, y);
        [self insertChild:resourceNode atIndex:0];
        [self.resourceNodes addObject:resourceNode];
        
    };
    
    if(self.gameParameters.resourceStepsCount.unsignedIntegerValue == 0)
    {
        if(self.step == 0)
        {
            for(NSUInteger i = 0; i < self.gameParameters.resourcesCount.unsignedIntegerValue; i++)
            {
                createResourceNodeBlock();
            }
        }
    }
    else if(self.step.unsignedIntegerValue % (self.gameParameters.resourceStepsCount.unsignedIntegerValue / (self.gameParameters.speed ? self.gameParameters.speed.unsignedIntegerValue : 1)) == 0)
    {
        if(self.resourceNodes.count < self.gameParameters.resourcesCount.unsignedIntegerValue)
        {
            createResourceNodeBlock();
        }
    }
}

- (NSArray<GWUnitAIFact *> *)factsForUnitNode:(GWUnitNode *)unitNode
{
    NSArray<GWUnitAIFact *> *knownFacts = [self factsFromUnitNode:unitNode];
    
    NSMutableArray<NSNumber *> *knownFactIdxs = [NSMutableArray array];
    NSNumber *knownFactsMaxStep = @0;
    for(GWUnitAIFact *fact in knownFacts)
    {
        [knownFactIdxs addObject:fact.idx];
        if(fact.step.unsignedIntegerValue > knownFactsMaxStep.unsignedIntegerValue)
        {
            knownFactsMaxStep = fact.step;
        }
    }
    
    NSMutableArray<GWUnitAIFact *> *facts = [NSMutableArray array];
    
    for(GWGameNode *node in self.children)
    {
        if(node == unitNode)
        {
            continue;
        }
        
        CGFloat diff = 0.0f;
        if([node isKindOfClass:[GWResourceNode class]])
        {
            diff = (GWConstantsResourceSize / 2);
        }
        else if([node isKindOfClass:[GWBaseNode class]])
        {
            diff = (GWConstantsBaseSize / 2);
        }
        else if([node isKindOfClass:[GWUnitNode class]])
        {
            diff = (GWConstantsUnitSize / 2);
        }
        
        if(CGPointDistance(node.position, unitNode.position) - diff < self.gameParameters.unitVisibilityRadius.floatValue || !self.gameParameters.unitVisibilityRadius)
        {
            GWUnitAIFact *fact = nil;
            if([node isKindOfClass:[GWResourceNode class]])
            {
                fact = [[GWUnitAIResourceFact alloc] initWithIdx:node.idx position:node.position step:node.step status:GWUnitAIResourceFactStatusFree];
            }
            else if([node isKindOfClass:[GWBaseNode class]])
            {
                GWBaseNode *baseNode = (GWBaseNode *)node;
                fact = [[GWUnitAIBaseFact alloc] initWithIdx:node.idx position:node.position step:node.step resourcesCount:baseNode.resourcesCount];
            }
            else if([node isKindOfClass:[GWUnitNode class]])
            {
                GWUnitNode *unitNode = (GWUnitNode *)node;
                [facts addObjectsFromArray:[self factsFromUnitNode:unitNode]];
            }
            if(fact)
            {
                [facts addObject:fact];
            }
        }
    }
    
    [facts filterUsingPredicate:[NSPredicate predicateWithFormat:@"not (idx in %@) || step.unsignedIntegerValue >= %@", knownFactIdxs, @0]];
    
    return facts;
}

- (NSArray<GWUnitAIFact *> *)factsFromUnitNode:(GWUnitNode *)unitNode
{
    if(!self.unitIdxsWithKnownFacts[unitNode.idx])
    {
        self.unitIdxsWithKnownFacts[unitNode.idx] = unitNode.unitAI.knownFacts;
    }
    return self.unitIdxsWithKnownFacts[unitNode.idx];
}

- (NSArray<GWUnitNode *> *)unitNodes
{
    NSMutableArray<GWUnitNode *> *unitNodes = [NSMutableArray array];
    for(GWUnitNode *unitNode in self.children)
    {
        if([unitNode isKindOfClass:[GWUnitNode class]])
        {
            [unitNodes addObject:unitNode];
        }
    }
    return unitNodes;
}

- (GWResourceNode *)resourceNodeWithIdx:(NSNumber *)idx
{
    for(GWResourceNode *resourceNode in self.children)
    {
        if([resourceNode isKindOfClass:[GWResourceNode class]] && [resourceNode.idx isEqualToNumber:idx])
        {
            return resourceNode;
        }
    }
    return nil;
}

- (GWBaseNode *)baseNodeWithIdx:(NSNumber *)idx
{
    for(GWBaseNode *baseNode in self.children)
    {
        if([baseNode isKindOfClass:[GWBaseNode class]] && [baseNode.idx isEqualToNumber:idx])
        {
            return baseNode;
        }
    }
    return nil;
}

- (void)performAIActionWithUnitNode:(GWUnitNode *)unitNode
{
    switch(unitNode.unitAI.AIAction.actionType)
    {
        case GWUnitActionTypeTake:
        {
            GWResourceNode *resourceNode = [self resourceNodeWithIdx:unitNode.unitAI.AIAction.idx];
            if(resourceNode)
            {
                GWUnitAIResourceFact *resourceFact = [[GWUnitAIResourceFact alloc] initWithIdx:resourceNode.idx position:resourceNode.position step:self.step status:GWUnitAIResourceFactStatusFree];
                
                [unitNode.unitAI takeResourceWithResourceFact:resourceFact];
                [resourceNode removeFromParent];
            }
            else
            {
                GWUnitAIResourceFact *resourceFact = [[GWUnitAIResourceFact alloc] initWithIdx:unitNode.unitAI.AIAction.idx position:unitNode.unitAI.AIAction.position step:self.step status:GWUnitAIResourceFactStatusTaken];
                
                [unitNode.unitAI learnFacts:@[resourceFact]];
            }
            break;
        }
            
        case GWUnitActionTypePut:
        {
            GWBaseNode *baseNode = [self baseNodeWithIdx:unitNode.unitAI.AIAction.idx];
            
            [baseNode putResourceWithStep:self.step];
            
            GWUnitAIBaseFact *baseFact = [[GWUnitAIBaseFact alloc] initWithIdx:baseNode.idx position:baseNode.position step:self.step resourcesCount:baseNode.resourcesCount];
            [unitNode.unitAI putResourceWithBaseFact:baseFact];
            break;
        }
            
        default:
            break;
    }
}

- (void)tryFinishGame
{
    NSUInteger puttedResourcesCount = 0;
    
    for(GWBaseNode *baseNode in self.children)
    {
        if([baseNode isKindOfClass:[GWBaseNode class]])
        {
            puttedResourcesCount += baseNode.resourcesCount;
        }
    }
    
    if(puttedResourcesCount == self.gameParameters.resourcesCount.unsignedIntegerValue)
    {
        self.gameResults.finishStep = self.step;
        
        NSMutableArray<GWUnitResults *> *unitsResults = [NSMutableArray array];
        for(GWUnitNode *unitNode in self.unitNodes)
        {
            GWUnitResults *unitResults = [[GWUnitResults alloc] init];
            unitResults.idx = unitNode.unitAI.idx;
            unitResults.puttedResourcesCount = @(unitNode.unitAI.puttedResourceFacts.count);
            
            [unitsResults addObject:unitResults];
        }
        [unitsResults sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"idx" ascending:YES]]];
        self.gameResults.unitsResults = unitsResults;
        
        NSMutableArray<GWBaseResults *> *basesResults = [NSMutableArray array];
        for(GWBaseNode *baseNode in self.children)
        {
            if([baseNode isKindOfClass:[GWBaseNode class]])
            {
                GWBaseResults *baseResults = [[GWBaseResults alloc] init];
                baseResults.idx = baseNode.idx;
                baseResults.resourcesCount = @(baseNode.resourcesCount);
                
                [basesResults addObject:baseResults];
            }
        }
        [basesResults sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"idx" ascending:YES]]];
        self.gameResults.basesResults = basesResults;
        
        if([self.gameSceneDelegate respondsToSelector:@selector(gameScene:didFinishWithGameResults:)])
        {
            [self.gameSceneDelegate gameScene:self didFinishWithGameResults:self.gameResults];
        }
    }
}

@end
