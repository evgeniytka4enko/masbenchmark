//
//  AppDelegate.h
//  GraduateWork
//
//  Created by Evgeniy on 19.01.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

