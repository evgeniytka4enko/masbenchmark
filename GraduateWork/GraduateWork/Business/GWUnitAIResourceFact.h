//
//  GWUnitAIResourceFact.h
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWUnitAIFact.h"

typedef NS_ENUM(NSUInteger, GWUnitAIResourceFactStatus)
{
    GWUnitAIResourceFactStatusFree,
    GWUnitAIResourceFactStatusBusy,
    GWUnitAIResourceFactStatusTaken,
    GWUnitAIResourceFactStatusPutted
};

@interface GWUnitAIResourceFact : GWUnitAIFact

@property (readonly, nonatomic) GWUnitAIResourceFactStatus status;

- (instancetype)initWithIdx:(NSNumber *)idx position:(CGPoint)position step:(NSNumber *)step status:(GWUnitAIResourceFactStatus)status;

@end
