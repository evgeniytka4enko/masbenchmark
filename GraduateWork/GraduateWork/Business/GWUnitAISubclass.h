//
//  GWUnitAISubclass.h
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GWUnitAI.h"
#import "GWUnitsAI.h"

@interface GWUnitAI (GWUnitAIProtected)

@property (strong, nonatomic) GWUnitsAI *unitsAI;

@property (strong, nonatomic) NSNumber *idx;
@property (strong, nonatomic) GWUnitAIAction *AIAction;
@property (strong, nonatomic) GWUnitAIResourceFact *takenResourceFact;

@end
