//
//  GWDecentralizedUnitAI.m
//  GraduateWork
//
//  Created by Evgeniy on 10.03.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWDecentralizedUnitAI.h"
#import "GWUnitAISubclass.h"

@interface GWDecentralizedUnitAI ()

@property (strong, nonatomic) NSMutableArray<GWUnitAIResourceFact *> *resourceFacts;
@property (strong, nonatomic) NSMutableArray<GWUnitAIBaseFact *> *baseFacts;
@property (strong, nonatomic) NSMutableArray<GWUnitAIResourceFact *> *myResourceFacts;
@property (strong, nonatomic) NSMutableSet<NSNumber *> *puttedResourceFactIdxs;

@property (strong, nonatomic) GWUnitAIResourceFact *expectedToTakeResourceFact;

@end

@implementation GWDecentralizedUnitAI

#pragma mark - Custom accessors

- (NSMutableArray<GWUnitAIResourceFact *> *)resourceFacts
{
    if(!_resourceFacts)
    {
        _resourceFacts = [NSMutableArray array];
    }
    return _resourceFacts;
}

- (NSMutableArray<GWUnitAIBaseFact *> *)baseFacts
{
    if(!_baseFacts)
    {
        _baseFacts = [NSMutableArray array];
    }
    return _baseFacts;
}

- (NSMutableArray<GWUnitAIResourceFact *> *)myResourceFacts
{
    if(!_myResourceFacts)
    {
        _myResourceFacts = [NSMutableArray array];
    }
    return _myResourceFacts;
}

- (NSMutableSet<NSNumber *> *)puttedResourceFactIdxs
{
    if(!_puttedResourceFactIdxs)
    {
        _puttedResourceFactIdxs = [NSMutableSet set];
    }
    return _puttedResourceFactIdxs;
}

#pragma mark - Overrides

- (NSArray<GWUnitAIFact *> *)knownFacts
{
    NSMutableArray<GWUnitAIFact *> *facts = [NSMutableArray array];
    [facts addObjectsFromArray:self.resourceFacts];
    [facts addObjectsFromArray:self.baseFacts];
    [facts addObjectsFromArray:self.myResourceFacts];
    return facts;
}

- (void)learnFacts:(NSArray<GWUnitAIFact *> *)facts
{
    [super learnFacts:facts];
    
    for(GWUnitAIFact *fact in facts)
    {
        BOOL finded = NO;
        NSUInteger updatedIndex = NSNotFound;
        NSUInteger removedIndex = NSNotFound;
        if([fact isKindOfClass:[GWUnitAIResourceFact class]])
        {
            if([self.puttedResourceFactIdxs containsObject:fact.idx])
            {
                continue;
            }
            
            GWUnitAIResourceFact *resourceFact = (GWUnitAIResourceFact *)fact;
            
            for(GWUnitAIResourceFact *knownResourceFact in self.resourceFacts)
            {
                if([resourceFact.idx isEqualToNumber:knownResourceFact.idx])
                {
                    finded = YES;
                    
                    if([self.expectedToTakeResourceFact.idx isEqualToNumber:knownResourceFact.idx] && resourceFact.status == GWUnitAIResourceFactStatusTaken)
                    {
                        self.expectedToTakeResourceFact = nil;
                        removedIndex = [self.resourceFacts indexOfObject:knownResourceFact];
                    }
                    else if(resourceFact.status != knownResourceFact.status && ![self.expectedToTakeResourceFact.idx isEqualToNumber:resourceFact.idx] && ![self.takenResourceFact.idx isEqualToNumber:resourceFact.idx])
                    {
                        if(resourceFact.status == GWUnitAIResourceFactStatusFree || resourceFact.status == GWUnitAIResourceFactStatusBusy)
                        {
                            updatedIndex = [self.resourceFacts indexOfObject:knownResourceFact];
                        }
                        else
                        {
                            removedIndex = [self.resourceFacts indexOfObject:knownResourceFact];
                            [self.puttedResourceFactIdxs addObject:knownResourceFact.idx];
                        }
                    }
                    break;
                }
            }
            if(!finded && (resourceFact.status == GWUnitAIResourceFactStatusFree || resourceFact.status == GWUnitAIResourceFactStatusBusy))
            {
                [self.resourceFacts addObject:resourceFact];
            }
            else if(updatedIndex != NSNotFound)
            {
                [self.resourceFacts replaceObjectAtIndex:updatedIndex withObject:resourceFact];
            }
            else if(removedIndex != NSNotFound)
            {
                [self.resourceFacts removeObjectAtIndex:removedIndex];
            }
        }
        else if([fact isKindOfClass:[GWUnitAIBaseFact class]])
        {
            GWUnitAIBaseFact *baseFact = (GWUnitAIBaseFact *)fact;
            
            for(GWUnitAIBaseFact *knownBaseFact in self.baseFacts)
            {
                if([baseFact.idx isEqualToNumber:knownBaseFact.idx])
                {
                    finded = YES;
                    
                    if(baseFact.step.unsignedIntegerValue > knownBaseFact.step.unsignedIntegerValue)
                    {
                        updatedIndex = [self.baseFacts indexOfObject:knownBaseFact];
                    }
                    
                    break;
                }
            }
            if(!finded)
            {
                [self.baseFacts addObject:baseFact];
            }
            else if(updatedIndex != NSNotFound)
            {
                [self.baseFacts replaceObjectAtIndex:updatedIndex withObject:baseFact];
            }
        }
    }
}

- (void)decide
{
    [super decide];
    
    if(self.resourceFacts.count == 0 && !self.takenResourceFact)
    {
        [self goLearn];
    }
    else if(!self.takenResourceFact)
    {
        GWUnitAIResourceFact *resourceFact = nil;
        
        for(GWUnitAIResourceFact *knownResourceFact in self.resourceFacts)
        {
            if(knownResourceFact.status == GWUnitAIResourceFactStatusFree || [self.expectedToTakeResourceFact.idx isEqualToNumber:knownResourceFact.idx])
            {
                if(!resourceFact || CGPointDistance(self.position, knownResourceFact.position) < CGPointDistance(self.position, resourceFact.position))
                {
                    resourceFact = knownResourceFact;
                }
            }
        }
        
        if(resourceFact)
        {
            if(![self.expectedToTakeResourceFact.idx isEqualToNumber:resourceFact.idx])
            {
                GWUnitAIResourceFact *oldExpectedResourceFact = nil;
                if(self.expectedToTakeResourceFact)
                {
                    oldExpectedResourceFact = [[GWUnitAIResourceFact alloc] initWithIdx:self.expectedToTakeResourceFact.idx position:self.expectedToTakeResourceFact.position step:self.step status:GWUnitAIResourceFactStatusFree];
                }
                
                GWUnitAIResourceFact *newResourceFact = [[GWUnitAIResourceFact alloc] initWithIdx:resourceFact.idx position:resourceFact.position step:self.step status:GWUnitAIResourceFactStatusBusy];
                [self learnFacts:@[newResourceFact]];
                
                self.expectedToTakeResourceFact = newResourceFact;
                
                if(oldExpectedResourceFact)
                {
                    [self learnFacts:@[oldExpectedResourceFact]];
                }
            }
            
            [self goTakeWithResourceFact:resourceFact];
        }
        else if(!self.expectedToTakeResourceFact)
        {
            self.expectedToTakeResourceFact = nil;
            [self goLearn];
        }
    }
    else
    {
        GWUnitAIBaseFact *baseFact = nil;
        
        for(GWUnitAIBaseFact *knownBaseFact in self.baseFacts)
        {
            if(!baseFact || CGPointDistance(self.position, knownBaseFact.position) < CGPointDistance(self.position, baseFact.position))
            {
                baseFact = knownBaseFact;
            }
        }
        
        if(baseFact)
        {
            [self goPutWithBaseFact:baseFact];
        }
        else
        {
            [self goLearn];
        }
    }
}

- (void)takeResourceWithResourceFact:(GWUnitAIResourceFact *)resourceFact
{
    [super takeResourceWithResourceFact:resourceFact];
    
    self.expectedToTakeResourceFact = nil;
    
    GWUnitAIResourceFact *newResourceFact = [[GWUnitAIResourceFact alloc] initWithIdx:resourceFact.idx position:resourceFact.position step:self.step status:GWUnitAIResourceFactStatusTaken];
    [self.myResourceFacts addObject:newResourceFact];
    [self learnFacts:@[newResourceFact]];
    
    self.takenResourceFact = newResourceFact;
}

- (void)putResourceWithBaseFact:(GWUnitAIBaseFact *)baseFact
{
    [super putResourceWithBaseFact:baseFact];
    
    [self learnFacts:@[baseFact]];
    
    self.takenResourceFact = nil;
}

#pragma mark - Private

- (void)goLearn
{
    if(!self.AIAction || self.AIAction.actionType != GWUnitActionTypeMove || CGPointDistance(self.AIAction.position, self.position) < 1.0f)
    {
        CGFloat x = (arc4random() % (NSUInteger)self.unitsAI.size.width);
        CGFloat y = (arc4random() % (NSUInteger)self.unitsAI.size.height);
        
        self.AIAction = [[GWUnitAIAction alloc] initWithActionType:GWUnitActionTypeMove idx:nil position:CGPointMake(x, y)];
    }
}

- (void)goTakeWithResourceFact:(GWUnitAIResourceFact *)resourceFact
{
    if(!self.AIAction || self.AIAction.actionType != GWUnitActionTypeTake || ![self.AIAction.idx isEqual:resourceFact.idx])
    {
        self.AIAction = [[GWUnitAIAction alloc] initWithActionType:GWUnitActionTypeTake idx:resourceFact.idx position:resourceFact.position];
    }
}

- (void)goPutWithBaseFact:(GWUnitAIBaseFact *)baseFact
{
    if(!self.AIAction || self.AIAction.actionType != GWUnitActionTypePut || ![self.AIAction.idx isEqual:baseFact.idx])
    {
        self.AIAction = [[GWUnitAIAction alloc] initWithActionType:GWUnitActionTypePut idx:baseFact.idx position:baseFact.position];
    }
}

@end
