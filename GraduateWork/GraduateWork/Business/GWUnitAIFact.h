//
//  GWUnitAIFact.h
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GWUnitAIFact : NSObject

@property (readonly, nonatomic) NSNumber *idx;
@property (readonly, nonatomic) CGPoint position;
@property (readonly, nonatomic) NSNumber *step;

- (instancetype)initWithIdx:(NSNumber *)idx position:(CGPoint)position step:(NSNumber *)step;

@end
