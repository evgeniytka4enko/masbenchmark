//
//  GWUnitsAI.m
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWUnitsAI.h"

@interface GWUnitsAI ()

@property (assign, nonatomic) CGSize size;

@end

@implementation GWUnitsAI

#pragma mark - Initialization

- (instancetype)initWithSize:(CGSize)size
{
    self = [super init];
    if(self)
    {
        self.size = size;
    }
    return self;
}

#pragma mark - Public

- (GWUnitAI *)createUnitAIWithIdx:(NSNumber *)idx
{
    return nil;
}

@end
