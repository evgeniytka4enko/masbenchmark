//
//  GWUnitsAI.h
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GWUnitAI.h"

@interface GWUnitsAI : NSObject

@property (readonly, nonatomic) CGSize size;

- (instancetype)initWithSize:(CGSize)size;

- (GWUnitAI *)createUnitAIWithIdx:(NSNumber *)idx;

@end
