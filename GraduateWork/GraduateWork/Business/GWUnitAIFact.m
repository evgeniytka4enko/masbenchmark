//
//  GWUnitAIFact.m
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWUnitAIFact.h"

@interface GWUnitAIFact ()

@property (strong, nonatomic) NSNumber *idx;
@property (assign, nonatomic) CGPoint position;
@property (strong, nonatomic) NSNumber *step;

@end

@implementation GWUnitAIFact

#pragma mark - Initialization

- (instancetype)initWithIdx:(NSNumber *)idx position:(CGPoint)position step:(NSNumber *)step
{
    self = [super init];
    if(self)
    {
        self.idx = idx;
        self.position = position;
        self.step = step;
    }
    return self;
}

@end
