//
//  GWUnitAIBaseFact.h
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWUnitAIFact.h"

@interface GWUnitAIBaseFact : GWUnitAIFact

@property (readonly, nonatomic) NSUInteger resourcesCount;

- (instancetype)initWithIdx:(NSNumber *)idx position:(CGPoint)position step:(NSNumber *)step resourcesCount:(NSUInteger)resourcesCount;

@end
