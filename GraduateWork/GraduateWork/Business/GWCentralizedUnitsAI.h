//
//  GWCentralizedUnitsAI.h
//  GraduateWork
//
//  Created by Evgeniy on 12.04.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWUnitsAI.h"
#import "GWCentralizedUnitAI.h"

@interface GWCentralizedUnitsAI : GWUnitsAI

- (NSArray<GWUnitAIFact *> *)knownFacts;

- (void)learnFacts:(NSArray<GWUnitAIFact *> *)facts unitAI:(GWCentralizedUnitAI *)unitAI;

- (void)decideWithUnitAI:(GWCentralizedUnitAI *)unitAI;

- (void)takeResourceWithResourceFact:(GWUnitAIResourceFact *)resourceFact unitAI:(GWCentralizedUnitAI *)unitAI;

- (void)putResourceWithBaseFact:(GWUnitAIBaseFact *)baseFact unitAI:(GWCentralizedUnitAI *)unitAI;

@end
