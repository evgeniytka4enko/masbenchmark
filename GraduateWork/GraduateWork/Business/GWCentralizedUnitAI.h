//
//  GWCentralizedUnitAI.h
//  GraduateWork
//
//  Created by Evgeniy on 12.04.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWUnitAI.h"

@interface GWCentralizedUnitAI : GWUnitAI

@property (strong, nonatomic) GWUnitAIResourceFact *resourceFact;

@end
