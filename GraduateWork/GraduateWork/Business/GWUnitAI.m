//
//  GWUnitAI.m
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWUnitAI.h"
#import "GWUnitsAI.h"

@interface GWUnitAI ()

@property (strong, nonatomic) GWUnitsAI *unitsAI;

@property (strong, nonatomic) NSNumber *idx;
@property (assign, nonatomic) CGPoint position;
@property (strong, nonatomic) NSNumber *step;
@property (strong, nonatomic) GWUnitAIAction *AIAction;
@property (strong, nonatomic) NSMutableArray<GWUnitAIResourceFact *> *puttedResourceFacts;
@property (strong, nonatomic) GWUnitAIResourceFact *takenResourceFact;

@end

@implementation GWUnitAI

#pragma mark - Initialization

- (instancetype)initWithUnitsAI:(GWUnitsAI *)unitsAI idx:(NSNumber *)idx
{
    self = [super init];
    if(self)
    {
        self.unitsAI = unitsAI;
        self.idx = idx;
    }
    return self;
}

#pragma mark - Custom accessors

- (NSArray<GWUnitAIFact *> *)knownFacts
{
    return nil;
}

- (NSArray<GWUnitAIResourceFact *> *)puttedResourceFacts
{
    if(!_puttedResourceFacts)
    {
        _puttedResourceFacts = [NSMutableArray array];
    }
    return _puttedResourceFacts;
}

#pragma mark - Public

- (void)learnFacts:(NSArray<GWUnitAIFact *> *)facts
{
    
}

- (void)takeResourceWithResourceFact:(GWUnitAIResourceFact *)resourceFact
{
    
}

- (void)putResourceWithBaseFact:(GWUnitAIBaseFact *)baseFact
{
    [(NSMutableArray *)self.puttedResourceFacts addObject:self.takenResourceFact];
}

- (void)setPosition:(CGPoint)position step:(NSNumber *)step
{
    self.position = position;
    self.step = step;
}

- (void)decide
{
    
}

@end
