//
//  GWUnitAIResourceFact.m
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWUnitAIResourceFact.h"

@interface GWUnitAIResourceFact ()

@property (assign, nonatomic) GWUnitAIResourceFactStatus status;

@end

@implementation GWUnitAIResourceFact

#pragma mark - Initialization

- (instancetype)initWithIdx:(NSNumber *)idx position:(CGPoint)position step:(NSNumber *)step status:(GWUnitAIResourceFactStatus)status
{
    self = [super initWithIdx:idx position:position step:step];
    if(self)
    {
        self.status = status;
    }
    return self;
}

@end
