//
//  GWUnitAIBaseFact.m
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWUnitAIBaseFact.h"

@interface GWUnitAIBaseFact ()

@property (assign, nonatomic) NSUInteger resourcesCount;

@end

@implementation GWUnitAIBaseFact

#pragma mark - Initialization

- (instancetype)initWithIdx:(NSNumber *)idx position:(CGPoint)position step:(NSNumber *)step resourcesCount:(NSUInteger)resourcesCount
{
    self = [super initWithIdx:idx position:position step:step];
    if(self)
    {
        self.resourcesCount = resourcesCount;
    }
    return self;
}

@end
