//
//  GWCentralizedUnitsAI.m
//  GraduateWork
//
//  Created by Evgeniy on 12.04.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWCentralizedUnitsAI.h"
#import "GWUnitAISubclass.h"

@interface GWCentralizedUnitsAI ()

@property (strong, nonatomic) NSMutableArray<GWCentralizedUnitAI *> *unitAIs;
@property (strong, nonatomic) NSMutableArray<GWUnitAIResourceFact *> *resourceFacts;
@property (strong, nonatomic) NSMutableArray<GWUnitAIBaseFact *> *baseFacts;

@end

@implementation GWCentralizedUnitsAI

#pragma mark - Overrides

- (GWUnitAI *)createUnitAIWithIdx:(NSNumber *)idx
{
    GWCentralizedUnitAI *unitAI = [[GWCentralizedUnitAI alloc] initWithUnitsAI:self idx:idx];
    [self.unitAIs addObject:unitAI];
    return unitAI;
}

#pragma mark - Custom accessors

- (NSMutableArray<GWCentralizedUnitAI *> *)unitAIs
{
    if(!_unitAIs)
    {
        _unitAIs = [NSMutableArray array];
    }
    return _unitAIs;
}

- (NSMutableArray<GWUnitAIResourceFact *> *)resourceFacts
{
    if(!_resourceFacts)
    {
        _resourceFacts = [NSMutableArray array];
    }
    return _resourceFacts;
}

- (NSMutableArray<GWUnitAIBaseFact *> *)baseFacts
{
    if(!_baseFacts)
    {
        _baseFacts = [NSMutableArray array];
    }
    return _baseFacts;
}

#pragma mark - Public

- (NSArray<GWUnitAIFact *> *)knownFacts
{
    NSMutableArray<GWUnitAIFact *> *facts = [NSMutableArray array];
    [facts addObjectsFromArray:self.resourceFacts];
    [facts addObjectsFromArray:self.baseFacts];
    return facts;
}

- (void)learnFacts:(NSArray<GWUnitAIFact *> *)facts unitAI:(GWCentralizedUnitAI *)unitAI
{
    if([self.unitAIs containsObject:unitAI])
    {
        for(GWUnitAIFact *fact in facts)
        {
            BOOL finded = NO;
            NSUInteger updatedIndex = NSNotFound;
            NSUInteger removedIndex = NSNotFound;
            if([fact isKindOfClass:[GWUnitAIResourceFact class]])
            {
                GWUnitAIResourceFact *resourceFact = (GWUnitAIResourceFact *)fact;
                
                for(GWUnitAIResourceFact *knownResourceFact in self.resourceFacts)
                {
                    if([resourceFact.idx isEqualToNumber:knownResourceFact.idx])
                    {
                        finded = YES;
                        if(resourceFact.status == GWUnitAIResourceFactStatusPutted)
                        {
                            removedIndex = [self.resourceFacts indexOfObject:knownResourceFact];
                        }
                        else if(resourceFact.status > knownResourceFact.status || resourceFact.status == GWUnitAIResourceFactStatusFree)
                        {
                            updatedIndex = [self.resourceFacts indexOfObject:knownResourceFact];
                        }
                        break;
                    }
                }
                if(!finded)
                {
                    [self.resourceFacts addObject:resourceFact];
                }
                else if(updatedIndex != NSNotFound)
                {
                    [self.resourceFacts replaceObjectAtIndex:updatedIndex withObject:resourceFact];
                }
                else if(removedIndex != NSNotFound)
                {
                    [self.resourceFacts removeObjectAtIndex:removedIndex];
                }
            }
            else if([fact isKindOfClass:[GWUnitAIBaseFact class]])
            {
                GWUnitAIBaseFact *baseFact = (GWUnitAIBaseFact *)fact;
                
                for(GWUnitAIBaseFact *knownBaseFact in self.baseFacts)
                {
                    if([baseFact.idx isEqualToNumber:knownBaseFact.idx])
                    {
                        finded = YES;
                        if(baseFact.resourcesCount > knownBaseFact.resourcesCount)
                        {
                            updatedIndex = [self.baseFacts indexOfObject:knownBaseFact];
                        }
                        break;
                    }
                }
                if(!finded)
                {
                    [self.baseFacts addObject:baseFact];
                }
                else if(updatedIndex != NSNotFound)
                {
                    [self.baseFacts replaceObjectAtIndex:updatedIndex withObject:baseFact];
                }
            }
        }
    }
}

- (void)decideWithUnitAI:(GWCentralizedUnitAI *)unitAI
{
    if([self.unitAIs containsObject:unitAI])
    {
        if(self.resourceFacts.count == 0)
        {
            [self goLearnWithUnitAI:unitAI];
        }
        else if(unitAI.resourceFact.status < GWUnitAIResourceFactStatusBusy)
        {
            GWUnitAIResourceFact *resourceFact = nil;
            CGFloat resourceOptimalDistance = CGFLOAT_MAX;
            
            for(GWUnitAIResourceFact *knownResourceFact in self.resourceFacts)
            {
                GWUnitAI *optimalUnitAI = nil;
                CGFloat optimalDistance = CGFLOAT_MAX;
                for(GWCentralizedUnitAI *knownUnitAI in self.unitAIs)
                {
                    if(knownUnitAI.AIAction.actionType != GWUnitActionTypePut)
                    {
                        if(CGPointDistance(knownUnitAI.position, knownResourceFact.position) < optimalDistance && (knownResourceFact.status == GWUnitAIResourceFactStatusFree || [knownUnitAI.resourceFact.idx isEqualToNumber:knownResourceFact.idx]))
                        {
                            optimalUnitAI = knownUnitAI;
                            optimalDistance = CGPointDistance(knownUnitAI.position, knownResourceFact.position);
                        }
                    }
                }
                
                if([optimalUnitAI.idx isEqualToNumber:unitAI.idx])
                {
                    if(CGPointDistance(unitAI.position, resourceFact.position) < resourceOptimalDistance)
                    {
                        resourceFact = knownResourceFact;
                        resourceOptimalDistance = CGPointDistance(unitAI.position, resourceFact.position);
                    }
                }
            }
            
            if(resourceFact)
            {
                if(unitAI.resourceFact && ![unitAI.resourceFact.idx isEqualToNumber:resourceFact.idx])
                {
                    GWUnitAIResourceFact *oldResourceFact = [[GWUnitAIResourceFact alloc] initWithIdx:unitAI.resourceFact.idx position:unitAI.resourceFact.position step:unitAI.step status:GWUnitAIResourceFactStatusFree];
                    [self learnFacts:@[oldResourceFact] unitAI:unitAI];
                }
                resourceFact = [[GWUnitAIResourceFact alloc] initWithIdx:resourceFact.idx position:resourceFact.position step:unitAI.step status:GWUnitAIResourceFactStatusBusy];
                
                for(GWCentralizedUnitAI *knownUnitAI in self.unitAIs)
                {
                    if([knownUnitAI.resourceFact.idx isEqualToNumber:resourceFact.idx])
                    {
                        knownUnitAI.resourceFact = nil;
                        knownUnitAI.AIAction = nil;
                    }
                }
                
                unitAI.resourceFact = resourceFact;
                [self learnFacts:@[resourceFact] unitAI:unitAI];
                
                [self goTakeWithResourceFact:resourceFact unitAI:unitAI];
            }
            else
            {
                [self goLearnWithUnitAI:unitAI];
            }
        }
        else if(unitAI.resourceFact.status == GWUnitAIResourceFactStatusTaken)
        {
            GWUnitAIBaseFact *baseFact = nil;
            
            for(GWUnitAIBaseFact *knownBaseFact in self.baseFacts)
            {
                if(!baseFact || CGPointDistance(unitAI.position, knownBaseFact.position) < CGPointDistance(unitAI.position, baseFact.position))
                {
                    baseFact = knownBaseFact;
                }
            }
            
            if(baseFact)
            {
                [self goPutWithBaseFact:baseFact unitAI:unitAI];
            }
            else
            {
                [self goLearnWithUnitAI:unitAI];
            }
        }
    }
}

- (void)takeResourceWithResourceFact:(GWUnitAIResourceFact *)resourceFact unitAI:(GWCentralizedUnitAI *)unitAI
{
    if([self.unitAIs containsObject:unitAI])
    {
        resourceFact = [[GWUnitAIResourceFact alloc] initWithIdx:resourceFact.idx position:resourceFact.position step:unitAI.step status:GWUnitAIResourceFactStatusTaken];
        unitAI.takenResourceFact = unitAI.resourceFact = resourceFact;
        [self learnFacts:@[resourceFact] unitAI:unitAI];
    }
}

- (void)putResourceWithBaseFact:(GWUnitAIBaseFact *)baseFact unitAI:(GWCentralizedUnitAI *)unitAI
{
    if([self.unitAIs containsObject:unitAI])
    {
        GWUnitAIResourceFact *resourceFact = [[GWUnitAIResourceFact alloc] initWithIdx:unitAI.resourceFact.idx position:unitAI.resourceFact.position step:unitAI.step status:GWUnitAIResourceFactStatusPutted];
        unitAI.takenResourceFact = unitAI.resourceFact = nil;
        [self learnFacts:@[baseFact,
                           resourceFact] unitAI:unitAI];
    }
}

#pragma mark - Private

- (void)goLearnWithUnitAI:(GWCentralizedUnitAI *)unitAI
{
    if(!unitAI.AIAction || unitAI.AIAction.actionType != GWUnitActionTypeMove || CGPointDistance(unitAI.AIAction.position, unitAI.position) < 1.0f)
    {
        CGFloat x = (arc4random() % (NSUInteger)self.size.width);
        CGFloat y = (arc4random() % (NSUInteger)self.size.height);
        
        unitAI.AIAction = [[GWUnitAIAction alloc] initWithActionType:GWUnitActionTypeMove idx:nil position:CGPointMake(x, y)];
    }
}

- (void)goTakeWithResourceFact:(GWUnitAIResourceFact *)resourceFact unitAI:(GWCentralizedUnitAI *)unitAI
{
    if(!unitAI.AIAction || unitAI.AIAction.actionType != GWUnitActionTypeTake || ![unitAI.AIAction.idx isEqual:resourceFact.idx])
    {
        unitAI.AIAction = [[GWUnitAIAction alloc] initWithActionType:GWUnitActionTypeTake idx:resourceFact.idx position:resourceFact.position];
    }
}

- (void)goPutWithBaseFact:(GWUnitAIBaseFact *)baseFact unitAI:(GWCentralizedUnitAI *)unitAI
{
    if(!unitAI.AIAction || unitAI.AIAction.actionType != GWUnitActionTypePut || ![unitAI.AIAction.idx isEqual:baseFact.idx])
    {
        unitAI.AIAction = [[GWUnitAIAction alloc] initWithActionType:GWUnitActionTypePut idx:baseFact.idx position:baseFact.position];
    }
}

@end
