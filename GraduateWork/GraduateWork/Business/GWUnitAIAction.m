//
//  GWUnitAIAction.m
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWUnitAIAction.h"

@interface GWUnitAIAction ()

@property (assign, nonatomic) GWUnitActionType actionType;
@property (strong, nonatomic) NSNumber *idx;
@property (assign, nonatomic) CGPoint position;

@end

@implementation GWUnitAIAction

#pragma mark - Initialization

- (instancetype)initWithActionType:(GWUnitActionType)actionType idx:(NSNumber *)idx position:(CGPoint)position
{
    self = [super init];
    if(self)
    {
        self.actionType = actionType;
        self.idx = idx;
        self.position = position;
    }
    return self;
}

@end
