//
//  GWGameResults.h
//  GraduateWork
//
//  Created by Evgeniy on 23.04.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GWUnitResults.h"
#import "GWBaseResults.h"

@interface GWGameResults : NSObject

@property (strong, nonatomic) NSNumber *finishStep;
@property (strong, nonatomic) NSNumber *summaryLearnFactsDuration;
@property (strong, nonatomic) NSNumber *summaryDecideDuration;
@property (strong, nonatomic) NSArray<GWUnitResults *> *unitsResults;
@property (strong, nonatomic) NSArray<GWBaseResults *> *basesResults;

@end
