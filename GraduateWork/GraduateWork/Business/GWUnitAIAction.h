//
//  GWUnitAIAction.h
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, GWUnitActionType)
{
    GWUnitActionTypeMove,
    GWUnitActionTypeTake,
    GWUnitActionTypePut
};

@interface GWUnitAIAction : NSObject

@property (readonly, nonatomic) GWUnitActionType actionType;
@property (readonly, nonatomic) NSNumber *idx;
@property (readonly, nonatomic) CGPoint position;

- (instancetype)initWithActionType:(GWUnitActionType)actionType idx:(NSNumber *)idx position:(CGPoint)position;

@end
