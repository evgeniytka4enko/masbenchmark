//
//  GWUnitAI.h
//  GraduateWork
//
//  Created by Evgeniy on 28.02.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GWUnitAIAction.h"
#import "GWUnitAIFact.h"
#import "GWUnitAIResourceFact.h"
#import "GWUnitAIBaseFact.h"

@class GWUnitsAI;

@interface GWUnitAI : NSObject

@property (readonly, nonatomic) GWUnitsAI *unitsAI;

@property (readonly, nonatomic) NSNumber *idx;
@property (readonly, nonatomic) CGPoint position;
@property (readonly, nonatomic) NSNumber *step;
@property (readonly, nonatomic) GWUnitAIAction *AIAction;
@property (readonly, nonatomic) NSArray<GWUnitAIFact *> *knownFacts;
@property (readonly, nonatomic) NSArray<GWUnitAIResourceFact *> *puttedResourceFacts;
@property (readonly, nonatomic) GWUnitAIResourceFact *takenResourceFact;

- (instancetype)initWithUnitsAI:(GWUnitsAI *)unitsAI idx:(NSNumber *)idx;

- (void)learnFacts:(NSArray<GWUnitAIFact *> *)facts;

- (void)takeResourceWithResourceFact:(GWUnitAIResourceFact *)resourceFact;

- (void)putResourceWithBaseFact:(GWUnitAIBaseFact *)baseFact;

- (void)setPosition:(CGPoint)position step:(NSNumber *)step;

- (void)decide;

@end
