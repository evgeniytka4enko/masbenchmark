//
//  GWBaseResults.h
//  GraduateWork
//
//  Created by Evgeniy on 23.04.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GWBaseResults : NSObject

@property (strong, nonatomic) NSNumber *idx;
@property (strong, nonatomic) NSNumber *resourcesCount;

@end
