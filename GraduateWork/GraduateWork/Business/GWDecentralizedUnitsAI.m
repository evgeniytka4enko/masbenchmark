//
//  GWDecentralizedUnitsAI.m
//  GraduateWork
//
//  Created by Evgeniy on 10.03.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWDecentralizedUnitsAI.h"

@implementation GWDecentralizedUnitsAI

#pragma mark - Overrides

- (GWUnitAI *)createUnitAIWithIdx:(NSNumber *)idx
{
    GWDecentralizedUnitAI *unitAI = [[GWDecentralizedUnitAI alloc] initWithUnitsAI:self idx:idx];
    return unitAI;
}

@end
