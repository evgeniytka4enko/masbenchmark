//
//  GWGameParameters.h
//  GraduateWork
//
//  Created by Evgeniy on 10.03.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GWUnitsAI.h"

@interface GWGameParameters : NSObject

@property (strong, nonatomic) GWUnitsAI *unitsAI;

@property (strong, nonatomic) NSNumber *mapIdx;
@property (strong, nonatomic) NSNumber *unitsCount;
@property (strong, nonatomic) NSNumber *basesCount;
@property (strong, nonatomic) NSNumber *unitVisibilityRadius;
@property (strong, nonatomic) NSNumber *resourcesCount;
@property (strong, nonatomic) NSNumber *resourceStepsCount;

@property (strong, nonatomic) NSNumber *speed;

@end
