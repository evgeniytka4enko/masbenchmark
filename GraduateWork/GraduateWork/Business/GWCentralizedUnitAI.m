//
//  GWCentralizedUnitAI.m
//  GraduateWork
//
//  Created by Evgeniy on 12.04.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import "GWCentralizedUnitAI.h"
#import "GWCentralizedUnitsAI.h"

@implementation GWCentralizedUnitAI

#pragma mark - Overrides

- (NSArray<GWUnitAIFact *> *)knownFacts
{
    return [((GWCentralizedUnitsAI *)self.unitsAI) knownFacts];
}

- (void)learnFacts:(NSArray<GWUnitAIFact *> *)facts
{
    [super learnFacts:facts];
    
    [((GWCentralizedUnitsAI *)self.unitsAI) learnFacts:facts unitAI:self];
}

- (void)decide
{
    [super decide];
    
    [((GWCentralizedUnitsAI *)self.unitsAI) decideWithUnitAI:self];
}

- (void)takeResourceWithResourceFact:(GWUnitAIResourceFact *)resourceFact
{
    [super takeResourceWithResourceFact:resourceFact];
    
    [((GWCentralizedUnitsAI *)self.unitsAI) takeResourceWithResourceFact:resourceFact unitAI:self];
}

- (void)putResourceWithBaseFact:(GWUnitAIBaseFact *)baseFact
{
    [super putResourceWithBaseFact:baseFact];
    
    [((GWCentralizedUnitsAI *)self.unitsAI) putResourceWithBaseFact:baseFact unitAI:self];
}

@end
