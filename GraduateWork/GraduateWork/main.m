//
//  main.m
//  GraduateWork
//
//  Created by Evgeniy on 19.01.16.
//  Copyright © 2016 evgeniytka4enko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
